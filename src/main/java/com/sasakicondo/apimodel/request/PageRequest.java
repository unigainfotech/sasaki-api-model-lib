package com.sasakicondo.apimodel.request;

import lombok.Data;

@Data
public class PageRequest<C> {
    private int size;
    private int pageNumber;
    private C criteria;
}
