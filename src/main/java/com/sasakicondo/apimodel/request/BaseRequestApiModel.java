package com.sasakicondo.apimodel.request;

import lombok.Data;

@Data
public abstract class BaseRequestApiModel {
    private String language;
    private String currency;
    private String token;
}
