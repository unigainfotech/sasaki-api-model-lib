package com.sasakicondo.apimodel.response;


import com.sasakicondo.apimodel.common.StatusApiModel;
import lombok.Data;

@Data
public class ResponseApiModel {
    private StatusApiModel status;
}
