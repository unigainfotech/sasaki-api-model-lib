package com.sasakicondo.apimodel.response;

import com.sasakicondo.apimodel.common.ActionApiModel;
import lombok.Data;

@Data
public class BaseActionResponseApiModel {
    private ActionApiModel action;
}
