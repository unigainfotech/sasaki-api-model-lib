package com.sasakicondo.apimodel.response;


import lombok.Data;

@Data
public class PageResponseApiModel<T> {
    private PageResponse<T> page;
}
