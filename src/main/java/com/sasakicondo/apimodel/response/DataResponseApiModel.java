package com.sasakicondo.apimodel.response;

import lombok.Data;

@Data
public class DataResponseApiModel<T> extends ResponseApiModel{
    private T data;
}
