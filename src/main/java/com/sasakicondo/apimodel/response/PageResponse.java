package com.sasakicondo.apimodel.response;

import lombok.Data;

import java.util.List;

@Data
public class PageResponse<T> {
    private List<T> contents;
    private int totalPages;
}
