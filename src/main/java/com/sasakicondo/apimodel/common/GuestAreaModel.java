package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class GuestAreaModel {
    private int guestAreaId;
    private String remark;
    private String guestAreaName;
    private List<GuestAreaDescModel> guestAreaDescriptions;
}
