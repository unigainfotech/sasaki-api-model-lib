package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class PropertyTypeDescModel {
    private int id;
    private String propertyTypeName;
    private LanguageModel language;
}
