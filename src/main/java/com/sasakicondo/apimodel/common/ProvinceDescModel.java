package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class ProvinceDescModel {

    private int id;
    private LanguageModel language;
    private String provinceName;
}
