package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class CurrencyExchangeRateModel {
    private int runNo;
    private CurrencyModel currency;
    private double rate;
}
