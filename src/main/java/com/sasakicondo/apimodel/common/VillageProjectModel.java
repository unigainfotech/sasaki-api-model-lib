package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class VillageProjectModel {
    private int projectId;
    private String lat;
    private String lng;
    private SubDistrictModel subDistrict;
    private DistrictModel district;
    private ProvinceModel province;
    private CountryModel country;
    private String zipCode;
    private String address;
    private FileDownloadModel projectLogo;
    private ProjectModel project;
    private List<VillageProjectDescModel> villageProjectDescriptions;
    private String projectName;
    private String titleInUrl;
    private String projectDetail;
    private String title;
    private List<VillageProjectGalleryModel> villageProjectGalleryModels;

    public SubDistrictModel getSubDistrict() {
        if (subDistrict == null) {
            return new SubDistrictModel();
        }
        return subDistrict;
    }

    public DistrictModel getDistrict() {
        if (district == null) {
            return new DistrictModel();
        }
        return district;
    }

    public ProjectModel getProject() {
        if (project == null) {
            return new ProjectModel();
        }
        return project;
    }

    public ProvinceModel getProvince() {
        if (province == null) {
            return new ProvinceModel();
        }
        return province;
    }

    public CountryModel getCountry() {
        if (country == null) {
            return new CountryModel();
        }
        return country;
    }
}
