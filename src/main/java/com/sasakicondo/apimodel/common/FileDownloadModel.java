package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class FileDownloadModel {
    private String fileName;
    private String fileUrl;
    private String fileSize;
    private String fileType;
}
