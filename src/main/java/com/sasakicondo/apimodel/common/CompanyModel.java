package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class CompanyModel {
    private int companyId;
    private String email;
    private String telephone;
    private String fax;
    private String mobile;
    private String hotLine;
    private double lat;
    private double lng;
    private String googleApiKey;
    private FileDownloadModel logo;
    private String metaTagGenerator;
    private String metaTagDescription;
    private String metaTagRight;
    private String metaTagKeyword;
    private String facebookUrl;
    private String twitterUrl;
    private String fanPageFacebookUrl;
    private String copyRight;
    private String androidAppUrl;
    private String iosAppUrl;
    private String companyTitle;
}
