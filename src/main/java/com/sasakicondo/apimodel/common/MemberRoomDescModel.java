package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberRoomDescModel {
    private int id;
    private String detail;
    private String name;
    private LanguageModel language;

}
