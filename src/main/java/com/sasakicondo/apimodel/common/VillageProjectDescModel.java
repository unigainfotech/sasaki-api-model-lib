package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class VillageProjectDescModel {
    private int id;
    private LanguageModel language;
    private String projectName;
    private String projectDetail;
}
