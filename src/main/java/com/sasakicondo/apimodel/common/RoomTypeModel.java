package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class RoomTypeModel {
    private int roomTypeId;
    private String remark;
    private String roomTypeName;
    private List<RoomTypeDescModel> roomTypeDescriptions;
}
