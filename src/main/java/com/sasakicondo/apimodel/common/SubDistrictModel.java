package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class SubDistrictModel {
    private int subDistrictId;
    List<SubDistrictDescModel> subDistrictDescriptions;
    private String subDistrictName;
}
