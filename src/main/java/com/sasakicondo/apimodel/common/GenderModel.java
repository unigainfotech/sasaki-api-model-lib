package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class GenderModel {
    private int genderId;
    private String genderName;
}
