package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.Set;

@Data
public class AreaModel {
    private int areaId;
    private String remark;
    private String areaName;
    private Set<AreaDescModel> areaDescriptions;
    private boolean check;
}
