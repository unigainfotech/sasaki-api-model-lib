package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberRoomItemModel {
    private int id;
    private RoomItemModel roomItem;
    private String remark;
}
