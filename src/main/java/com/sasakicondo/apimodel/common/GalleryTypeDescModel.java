package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class GalleryTypeDescModel {
    private int id;
    private String galleryTypeName;
    private LanguageModel language;
}
