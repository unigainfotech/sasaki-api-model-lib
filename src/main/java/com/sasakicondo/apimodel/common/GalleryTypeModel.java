package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class GalleryTypeModel {
    private int galleryTypeId;
    private String remark;
    private String galleryTypeName;
    private List<GalleryTypeDescModel> galleryTypeDescriptions;
}
