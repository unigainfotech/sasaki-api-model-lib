package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class SubDistrictDescModel {

    private int id;
    private LanguageModel language;
    private String subDistrictName;

}
