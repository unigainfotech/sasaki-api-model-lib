package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class DistrictModel {

    private int districtId;
    private String districtName;
    private List<DistrictDescModel> districtDescriptions;
    private List<SubDistrictModel> subDistricts;
}
