package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class DistrictDescModel {

    private int id;
    private LanguageModel language;
    private String districtName;
}
