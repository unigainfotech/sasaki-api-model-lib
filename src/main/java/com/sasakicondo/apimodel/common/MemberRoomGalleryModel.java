package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberRoomGalleryModel {
    private int id;
    private FileDownloadModel picture;
}
