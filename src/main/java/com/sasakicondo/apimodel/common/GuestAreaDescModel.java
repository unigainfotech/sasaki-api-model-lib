package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class GuestAreaDescModel {
    private int id;
    private String guestAreaName;
    private LanguageModel language;
}
