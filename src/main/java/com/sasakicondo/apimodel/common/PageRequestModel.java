package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class PageRequestModel {
    private int size;
    private int pageNumber;
}
