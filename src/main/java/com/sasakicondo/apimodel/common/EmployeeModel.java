package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class EmployeeModel {
    private int employeeId;
    private String firstName;
    private String lastName;
    private String mobile;
    private String email;
    private String idCard;
    private String picture;
    private PositionModel position;
    private LanguageModel language;
}
