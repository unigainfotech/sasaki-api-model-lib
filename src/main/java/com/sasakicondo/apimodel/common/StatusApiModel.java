package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class StatusApiModel {
    private boolean success;
    private String code;
    private String title;
    private String description;
    private String devMessage;
}
