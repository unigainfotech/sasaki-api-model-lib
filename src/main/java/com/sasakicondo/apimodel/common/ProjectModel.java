package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class ProjectModel {

    private int projectId;
    private String titleInUrl;
    private CountryModel country;
    private String projectName;
    private List<ProjectDescModel> projectDescriptions;

}
