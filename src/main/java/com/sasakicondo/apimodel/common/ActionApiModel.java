package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class ActionApiModel {
    private boolean success;
    private int statusCode;
    private String message;
}
