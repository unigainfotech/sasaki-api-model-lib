package com.sasakicondo.apimodel.common;

import lombok.Data;



@Data
public class ProjectDescModel {
    private int id;
    private LanguageModel language;
    private String projectName;
}
