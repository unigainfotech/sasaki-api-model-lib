package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class CountryModel {
    private int countryId;
    private String countryName;
    private String titleInUrl;
    private FileDownloadModel countryPicture;
    private String mobilePattern;
    private List<ProvinceModel> provinces;
}
