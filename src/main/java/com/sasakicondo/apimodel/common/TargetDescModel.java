package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class TargetDescModel {

    private int id;
    private String targetName;
    private LanguageModel language;
}
