package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class RoomTypeDescModel {
    private int id;
    private String roomTypeName;
    private LanguageModel language;
}
