package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberModel {

    private int memberId;
    private String firstName;
    private String lastName;
    private String email;
    private String idCard;
    private String mobile;
    private String address;
    private String birthDate;
    private FileDownloadModel profilePicture;
    private FileDownloadModel idCardAttachFile;
    private String registerDate;
    private String remark;
    private String zipCode;
    private GenderModel gender;
    private CountryModel country;
    private ProvinceModel province;
    private DistrictModel district;
    private SubDistrictModel subDistrict;
    private MemberTypeModel memberType;
    private LanguageModel language;
    private StatusModel status;

    public String fullname() {
        return firstName + " " + lastName;
    }
}
