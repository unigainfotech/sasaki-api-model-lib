package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class RoomItemModel {

    private int roomItemId;
    private FileDownloadModel roomItemPicture;
    private List<RoomItemDescModel> roomItemDescriptions;
    private String roomItemName;
    private String roomItemDetail;
    private boolean check;
}
