package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class RoomItemDescModel {
    private int id;
    private String roomItemName;
    private String roomItemDetail;
    private LanguageModel language;

}
