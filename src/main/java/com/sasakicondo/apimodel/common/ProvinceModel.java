package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class ProvinceModel {
    private int provinceId;
    private CountryModel country;
    private List<DistrictModel> districts;
    private List<ProvinceDescModel> provinceDescriptions;
    private String provinceName;
    private String titleInUrl;
}
