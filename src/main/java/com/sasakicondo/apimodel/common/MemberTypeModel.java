package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberTypeModel {
    private int memberTypeId;
    private String memberTypeName;
}
