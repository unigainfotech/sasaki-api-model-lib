package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class AreaDescModel {
    private int id;
    private String areaName;
    private LanguageModel language;

}
