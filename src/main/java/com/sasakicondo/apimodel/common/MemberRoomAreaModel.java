package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberRoomAreaModel {

    private int id;
    private AreaModel area;
    private String remark;
}
