package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class CompanyTopicModel {
    private int runNo;
    private TopicModel topic;
    private String topicDesc;
    private String detail;
    private String remark;
}
