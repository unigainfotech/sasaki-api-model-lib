package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class ReserveModel {
    private int reserveId;
    private String reserveNo;
    private String reserveDate;
    private MemberRoomModel memberRoom;
    private String checkInDateTime;
    private String checkoutDateTime;
    private String actualCheckInDateTime;
    private String actualCheckoutDateTime;
    private String walkInDateTime;
    private MemberModel member;
    private StatusModel reserveStatus;
    private MemberModel dealer;
    private StatusModel dealerStatus;
    private StatusModel memberPaymentStatus;
    private String memberPaymentDateTime;
    private double memberPaymentAmount;
    private String memberPaymentRemark;
    private StatusModel dealerPaymentStatus;
    private String dealerPaymentDateTime;
    private double dealerPaymentAmount;
    private String dealerPaymentRemark;
    private String memberEmail;
    private String memberTel;
    private String reserveRemark;
    private double reservePrice;
    private ExchangeRateModel exchange;
    private int ratingScore;
    private String commentTitle;
    private String commentDetail;
    private int adultAmount;
    private int childAmount;
    private StatusModel lastStatus;

    public MemberRoomModel getMemberRoom() {
        if (memberRoom == null) {
            return new MemberRoomModel();
        }
        return memberRoom;
    }

    public MemberModel getMember() {
        if (member == null) {
            return new MemberModel();
        }
        return member;
    }

    public StatusModel getReserveStatus() {
        if (reserveStatus == null) {
            return new StatusModel();
        }
        return reserveStatus;
    }

    public MemberModel getDealer() {
        if (dealer == null) {
            return new MemberModel();
        }
        return dealer;
    }

    public StatusModel getDealerStatus() {
        if (dealerStatus == null) {
            return new StatusModel();
        }
        return dealerStatus;
    }

    public StatusModel getMemberPaymentStatus() {
        if (memberPaymentStatus == null) {
            return new StatusModel();
        }
        return memberPaymentStatus;
    }

    public StatusModel getDealerPaymentStatus() {
        if (dealerPaymentStatus == null) {
            return new StatusModel();
        }
        return dealerPaymentStatus;
    }

    public ExchangeRateModel getExchange() {
        if (exchange == null) {
            return new ExchangeRateModel();
        }
        return exchange;
    }

    public StatusModel getLastStatus() {
        if (lastStatus == null) {
            return new StatusModel();
        }
        return lastStatus;
    }
}
