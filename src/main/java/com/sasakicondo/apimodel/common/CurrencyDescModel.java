package com.sasakicondo.apimodel.common;


import lombok.Data;

@Data
public class CurrencyDescModel {
    private int id;
    private String currencyName;
    private LanguageModel language;

}
