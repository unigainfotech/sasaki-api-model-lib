package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class RoomReviewModel {
    private MemberModel member;
    private String title;
    private String detail;
    private int ratingScore;

}
