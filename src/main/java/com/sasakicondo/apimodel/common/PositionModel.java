package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class PositionModel {

    private int positionId;
    private String positionName;
}
