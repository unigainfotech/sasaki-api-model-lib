package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class CurrencyModel {
    private int currencyId;
    private String currencyName;
    private String currencyCode;
    private String currencySign;
    private double rate;
    private List<CurrencyDescModel> currencyDescriptions;

}
