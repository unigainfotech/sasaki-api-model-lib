package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class StatusModel {

    private int statusId;
    private String statusName;
    private StatusGroupModel statusGroup;
    private List<StatusDescModel> statusDescriptions;
}
