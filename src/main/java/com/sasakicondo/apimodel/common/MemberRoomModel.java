package com.sasakicondo.apimodel.common;

import lombok.Data;


import java.util.List;


@Data
public class MemberRoomModel {
    private int roomNo;
    private int ratingScore;
    private int maxRatingScore;
    private String titleInUrl;
    private MemberModel member;
    private StatusModel approveStatus;
    private String approveDate;
    private EmployeeModel approveBy;
    private int percentSuccess;
    private CurrencyModel currency;
    private double estimatePrice;
    private String roomNoCode;
    private String detail;
    private String name;
    private List<MemberRoomDescModel> memberRoomDescriptions;
    private MemberRoomInfoModel memberRoomInfoEntity;
    private String floor;
    private VillageProjectModel villageProject;
    private double priceForRent;
    private StatusModel saveStatus;
    private String registerDate;
    private ExchangeRateModel exchangeRate;
    private List<MemberRoomAreaModel> memberRoomAreas;
    private List<MemberRoomTargetModel> memberRoomTargets;
    private List<RoomItemTypeModel> roomItemTypeModels;
    private List<MemberRoomGalleryModel> memberRoomGalleryModels;
    private FileDownloadModel pictureVillageOfRoom;
    private String shortAddress;
    private int totalReview;
    private String agentEmail;
    private String agentTel;
    private List<RoomReviewModel> roomReviewModels;
    private String roomSize;
    public int getMaxRatingScore() {
        return 5;
    }

    public MemberModel getMember() {
        if (member == null) {
            return new MemberModel();
        }
        return member;
    }

    public StatusModel getApproveStatus() {
        if (approveStatus == null) {
            return new StatusModel();
        }
        return approveStatus;
    }

    public EmployeeModel getApproveBy() {
        if (approveBy == null) {
            return new EmployeeModel();
        }
        return approveBy;
    }

    public CurrencyModel getCurrency() {
        if (currency == null) {
            return new CurrencyModel();
        }
        return currency;
    }

    public VillageProjectModel getVillageProject() {
        if (villageProject == null) {
            return new VillageProjectModel();
        }
        return villageProject;
    }
}
