package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class VillageProjectGalleryModel {
    private int id;
    private GalleryTypeModel galleryType;
    private FileDownloadModel picture;
}
