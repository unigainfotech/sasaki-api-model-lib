package com.sasakicondo.apimodel.common;


import lombok.Data;

@Data
public class StatusGroupModel {
    private int statusGroupId;
    private String statusGroupName;
}
