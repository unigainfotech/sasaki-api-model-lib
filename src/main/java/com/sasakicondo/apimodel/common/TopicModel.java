package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class TopicModel {
    private int topicId;
    private String topicName;
}
