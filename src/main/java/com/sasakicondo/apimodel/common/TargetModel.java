package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class TargetModel {
    private int targetId;
    private String remark;
    private List<TargetDescModel> targetDescriptions;
    private boolean check;
    private String targetName;
}
