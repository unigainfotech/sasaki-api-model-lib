package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class PropertyTypeModel {
    private int propertyTypeId;
    private String remark;
    private String propertyTypeName;
    private List<PropertyTypeModel> propertyTypeDescriptions;
}
