package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class StatusDescModel {
    private int id;
    private LanguageModel language;
    private String statusName;
}
