package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberRoomTargetModel {
    private int id;
    private TargetModel target;
    private String remark;
}
