package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;


@Data
public class ExchangeRateModel {
    private int exchangeId;
    private String exchangeDate;
    private List<CurrencyExchangeRateModel> currencyExchangeRates;
}
