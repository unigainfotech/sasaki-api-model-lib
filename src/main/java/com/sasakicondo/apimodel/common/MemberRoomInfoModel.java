package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class MemberRoomInfoModel {

    private RoomTypeModel roomType;
    private PropertyTypeModel propertyType;
    private int guestAmount;
    private int amountUsingRoom;
    private int amountUsingBed;
    private int amountBathroom;
    private GuestAreaModel guestArea;
}
