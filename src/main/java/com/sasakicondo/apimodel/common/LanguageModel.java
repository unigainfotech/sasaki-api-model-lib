package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class LanguageModel {
    private String languageId;
    private String languageName;
    private FileDownloadModel languagePicture;
    private int priority;
}
