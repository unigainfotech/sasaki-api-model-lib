package com.sasakicondo.apimodel.common;

import lombok.Data;

@Data
public class RoomItemTypeDescModel {

    private int id;
    private String roomItemTypeName;
    private LanguageModel language;

}
