package com.sasakicondo.apimodel.common;

import lombok.Data;

import java.util.List;

@Data
public class RoomItemTypeModel {
    private int roomItemTypeId;
    private FileDownloadModel roomItemTypePicture;
    private String roomItemTypeName;
    private List<RoomItemTypeDescModel> roomItemTypeDescriptions;
    private List<RoomItemModel> roomItemModels;
}
