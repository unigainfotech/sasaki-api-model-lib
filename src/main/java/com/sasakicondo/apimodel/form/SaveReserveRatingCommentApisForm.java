package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class SaveReserveRatingCommentApisForm {

    @Data
    public static class SaveRatingParam {
        private int memberId;
        private int ratingScore;
        private String title;
        private String detail;
    }

    @Data
    public static class Request extends BaseRequestApiModel {
        private SaveRatingParam ratingParam;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
