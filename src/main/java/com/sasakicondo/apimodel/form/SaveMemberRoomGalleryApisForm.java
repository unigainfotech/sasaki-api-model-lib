package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomGalleryModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

import java.util.List;

public class SaveMemberRoomGalleryApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private List<MemberRoomGalleryModel> memberRoomGalleryModels;
        private int roomNo;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
