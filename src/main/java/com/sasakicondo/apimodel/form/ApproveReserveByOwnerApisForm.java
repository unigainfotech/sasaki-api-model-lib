package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.StatusModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class ApproveReserveByOwnerApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int reserveId;
        StatusModel dealerStatus;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
