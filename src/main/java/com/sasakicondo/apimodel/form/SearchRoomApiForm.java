package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.request.PageRequest;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

public class SearchRoomApiForm {


    @Data
    public static class SearchFilter {
        private String textSearch;
        private int amountGuests;
        private String checkInDate;
        private int amountMonthOfRental;
        private SortFilter.Sort sort;
    }

    @Data
    public static class Request extends BaseRequestApiModel {
        private PageRequest<SearchFilter> pageRequest;
    }

    @Data
    public static class Response extends PageResponseApiModel<String> {

    }
}
