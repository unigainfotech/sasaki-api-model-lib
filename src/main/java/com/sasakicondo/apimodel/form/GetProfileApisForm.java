package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class GetProfileApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private int memberId;
    }

    @Data
    public static class Response {
        private MemberModel memberItem;
    }
}
