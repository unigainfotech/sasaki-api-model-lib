package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomItemModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

import java.util.List;

public class SaveMemberRoomItemApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private List<MemberRoomItemModel> memberRoomItemModels;
        private int roomNo;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
