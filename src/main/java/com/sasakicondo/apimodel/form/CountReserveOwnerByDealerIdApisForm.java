package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class CountReserveOwnerByDealerIdApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private int memberId;
    }

    @Data
    public static class Response {
        private int count;
    }
}
