package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.RoomItemTypeModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

import java.util.List;

public class GetRoomItemListApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
    }

    @Data
    public static class Response extends PageResponseApiModel<String> {
        private List<RoomItemTypeModel> roomItemTypeModels;
    }
}
