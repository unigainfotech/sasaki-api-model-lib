package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.ReserveModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class PrepareReserveApisForm {

    @Data
    public static class SearchFilter {
        private int roomNo;
        private String checkInDate;
        private int rentalMonth;
        private int guestAmount;
        private int memberId;

    }


    @Data
    public static class Request extends BaseRequestApiModel {
        private PrepareReserveApisForm.SearchFilter searchFilter;
    }

    @Data
    public static class Response {
        private ReserveModel reserveModel;
    }

}
