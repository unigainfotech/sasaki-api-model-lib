package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.ReserveModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.request.PageRequest;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

public class GetReserveHistoryPageFilterApisForm {

    @Data
    public static class SearchFilter {
        private int memberId;
        private String checkInDate;
        private String roomName;
    }

    @Data
    public static class Request extends BaseRequestApiModel {
        private PageRequest<SearchFilter> page;
    }

    @Data
    public static class Response extends PageResponseApiModel<ReserveModel> {

    }
}
