package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class GetMemberRoomByIdApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int roomNo;
    }

    @Data
    public static class Response {
        private MemberRoomModel memberRoomModel;
    }
}
