package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class DeleteMemberRoomApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int roomNo;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
