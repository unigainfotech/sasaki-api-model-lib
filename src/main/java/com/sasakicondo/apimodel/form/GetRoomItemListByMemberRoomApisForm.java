package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.RoomItemTypeModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

import java.util.List;

public class GetRoomItemListByMemberRoomApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int roomNo;
    }

    @Data
    public static class Response extends PageResponseApiModel<String> {
        private List<RoomItemTypeModel> roomItemTypeModels;
    }
}
