package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.CountryModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetCountryListApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {

    }

    @Data
    public static class Response {
        private List<CountryModel> countryListItem;
    }
}
