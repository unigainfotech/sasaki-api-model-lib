package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.ProvinceModel;
import com.sasakicondo.apimodel.common.VillageProjectModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetTopProvincesApisForm {

    @Data
    public static class ProvinceVillages {
       private ProvinceModel provinceModel;
       private List<VillageProjectModel> villageProjectList;
    }

    @Data
    public static class Request extends BaseRequestApiModel {

    }

    @Data
    public static class Response {
        private List<ProvinceVillages> list;
    }
}
