package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.ProjectModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetProjectMasterListApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private int countryId;
    }

    @Data
    public static class Response {
        private List<ProjectModel> projectListItem;
    }
}
