package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.VillageProjectModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetVillageListApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private int projectId;
    }

    @Data
    public static class Response {
        private List<VillageProjectModel> villageProjectListItem;

    }
}
