package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.CompanyModel;
import com.sasakicondo.apimodel.common.CurrencyModel;
import com.sasakicondo.apimodel.common.LanguageModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;


public class GetCompanyProfileApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {

    }

    @Data
    public static class Response {
        private CompanyModel company;
        private List<LanguageModel> languageListItem;
        private List<CurrencyModel>	currencyListItem;
    }
}
