package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class SaveMemberRoomApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private MemberRoomModel memberRoomModel;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
