package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.CompanyTopicModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class GetCompanyPolicyApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {

    }

    @Data
    public static class Response {
        private CompanyTopicModel companyTopicItem;
    }
}
