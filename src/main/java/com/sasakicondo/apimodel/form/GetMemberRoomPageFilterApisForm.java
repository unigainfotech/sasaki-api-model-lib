package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.request.PageRequest;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

public class GetMemberRoomPageFilterApisForm {

    @Data
    public static class SearchFilter {
        private int countryId;
        private int projectMasterId;
        private int villageProjectId;
        private String roomName;
        private String checkInDate;
        private int rentalMonth;
        private int guestAmount;
        private SortFilter sort;
    }

    @Data
    public static class Request extends BaseRequestApiModel {
        PageRequest<SearchFilter> page;
    }

    @Data
    public static class Response extends PageResponseApiModel<MemberRoomModel> {

    }

}
