package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.RoomTypeModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetRoomTypeListApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {

    }

    @Data
    public static class Response {
        private List<RoomTypeModel> roomTypeModels;
    }
}
