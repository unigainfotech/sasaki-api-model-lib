package com.sasakicondo.apimodel.form;

public class SortFilter {

    public enum Sort {
        PriceLowToHeight,
        PriceHeightToLow,
        Ranking
    }
}
