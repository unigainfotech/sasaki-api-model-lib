package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.TargetModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

import java.util.List;

public class GetTargetListApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
    }

    @Data
    public static class Response extends PageResponseApiModel<String> {
        private List<TargetModel> targetModels;
    }
}
