package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.VillageProjectModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

public class GetVillageProjectByIdApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int projectId;
    }

    @Data
    public static class Response extends PageResponseApiModel<String> {
        private VillageProjectModel villageProjectItem;
    }
}
