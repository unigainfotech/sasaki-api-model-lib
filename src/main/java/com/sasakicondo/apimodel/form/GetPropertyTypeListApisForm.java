package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.PropertyTypeModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetPropertyTypeListApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {

    }

    @Data
    public static class Response {
        private List<PropertyTypeModel> propertyTypeModels;
    }
}
