package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class ForgotPasswordApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private String email;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
