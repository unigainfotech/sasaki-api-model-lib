package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.ReserveModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class GetReserveByIdApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private int reserveId;
    }

    @Data
    public static class Response {
        private ReserveModel reserveModel;
    }
}
