package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

public class CheckReserveApisForm {

    @Data
    private static class CheckFilter {
        private int countryId;
        private int projectMasterId;
        private int villageProjectId;
        private String roomName;
        private String checkInDate;
        private int rentalMonth;
        private int guestAmount;
    }

    @Data
    public static class Request extends BaseRequestApiModel {
        private CheckFilter check;
    }

    @Data
    public static class Response {
        private boolean isReserve;
    }
}
