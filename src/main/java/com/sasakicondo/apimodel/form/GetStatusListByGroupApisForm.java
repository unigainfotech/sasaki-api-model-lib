package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.StatusModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetStatusListByGroupApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int statusGroupId;
    }

    @Data
    public static class Response {
        private List<StatusModel> statusModels;
    }
}
