package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class DeleteMemberRoomGalleryApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int roomNo;
        private int id;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {

    }
}
