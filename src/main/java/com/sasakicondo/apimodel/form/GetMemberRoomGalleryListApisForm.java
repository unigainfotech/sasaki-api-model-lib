package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomGalleryModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetMemberRoomGalleryListApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int roomNo;
    }

    @Data
    public static class Response {
        private List<MemberRoomGalleryModel> memberRoomGalleryModels;
    }
}
