package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberRoomModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetMemberRoomListApisForm {

    @Data
    public static class Request extends BaseRequestApiModel {
        private int villageProjectId;
    }

    @Data
    public static class Response {
        private List<MemberRoomModel> memberRoomModels;
    }
}
