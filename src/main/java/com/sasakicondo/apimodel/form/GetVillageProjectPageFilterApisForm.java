package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.VillageProjectModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.request.PageRequest;
import com.sasakicondo.apimodel.response.PageResponseApiModel;
import lombok.Data;

public class GetVillageProjectPageFilterApisForm {

    @Data
    public static class SearchFilter {
        private String textSearch;
    }

    @Data
    public static class Request extends BaseRequestApiModel {
        PageRequest<SearchFilter> page;
    }

    @Data
    public static class Response extends PageResponseApiModel<VillageProjectModel> {

    }
}
