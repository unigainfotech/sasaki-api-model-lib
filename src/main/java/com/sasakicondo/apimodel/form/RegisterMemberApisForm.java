package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.MemberModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.BaseActionResponseApiModel;
import lombok.Data;

public class RegisterMemberApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private MemberModel memberItem;
        private String password;
    }

    @Data
    public static class Response extends BaseActionResponseApiModel {
        private MemberModel memberItem;

    }

}
