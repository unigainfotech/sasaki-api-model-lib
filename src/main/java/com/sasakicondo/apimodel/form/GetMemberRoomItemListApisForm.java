package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.RoomItemTypeModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import lombok.Data;

import java.util.List;

public class GetMemberRoomItemListApisForm {
    @Data
    public static class Request extends BaseRequestApiModel {
        private int roomNo;
    }

    @Data
    public static class Response {
        private List<RoomItemTypeModel> roomItemTypeModels;
    }
}
