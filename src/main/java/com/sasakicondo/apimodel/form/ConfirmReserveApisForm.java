package com.sasakicondo.apimodel.form;

import com.sasakicondo.apimodel.common.ReserveModel;
import com.sasakicondo.apimodel.request.BaseRequestApiModel;
import com.sasakicondo.apimodel.response.ActionResponseApiModel;
import lombok.Data;

public class ConfirmReserveApisForm {
    
    @Data
    public static class Request extends BaseRequestApiModel {
        private ReserveModel reserveModel;
    }

    @Data
    public static class Response extends ActionResponseApiModel {

    }
}
